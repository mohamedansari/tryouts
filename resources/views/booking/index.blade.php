@extends('includes.master')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

@endsection
@section('content')
<div class="jumbotron">
      <div class="container">


        <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Pitch Id</th>
                <th>Price</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Pitch Id</th>
              <th>Price</th>
            </tr>
        </tfoot>
        <tbody>
          @foreach($bookings as $booking)
            <tr>
                <td>{{$booking->id}}</td>
                <td>{{$booking->user->first_name}} {{$booking->user->last_name}}</td>
                <td>{{$booking->pitch_id}} </td>
                <td>{{$booking->price}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
  </div>
</div>

@endsection
@section('js')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
  </script>
@endsection
