@extends('includes.master')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

@endsection
@section('content')
<div class="jumbotron">
      <div class="container">


        <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Phone Number</th>
                <th>Country</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Pitch Id</th>
              <th>Price</th>
            </tr>
        </tfoot>
        <tbody>
          @foreach($users as $user)
            <tr>
                <td>{{$user->first_name}}</td>
                <td> {{$user->last_name}}</td>
                <td>{{$user->phone}} </td>
                <td>{{$user->country->name}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
  </div>
</div>

@endsection
@section('js')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
  </script>
@endsection
