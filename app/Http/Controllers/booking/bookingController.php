<?php

namespace App\Http\Controllers\booking;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Booking;
use View;
class bookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
      //retrieve all bookings
      $bookings = Booking::get()->all();
      return View::make('booking.index', array(
          'bookings' => $bookings,
      ));
    }

    public function raw()
    {
      //get and json encode the database records (first_name & last name as name from user tables  , phone from user tables, price from booking table)
      $bookings = json_encode(\DB::select('select bookings.id,  CONCAT(users.first_name , " ", users.last_name) as name, users.phone, bookings.price as price  from users, bookings where users.id = bookings.user_id'));
      return View::make('booking.raw', array(
          'bookings' => $bookings,
      ));
    }



}
