<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
            'first_name' => 'mohamed',
            'last_name' => 'ansari',
            'phone' => sprintf('973%d',rand(1000000, 99999999)),
            'country_id' => '1',
            'email' => 'test@test.com',
            'password' => bcrypt('111222'),
        ]);
    }
}
