<?php

use Illuminate\Database\Seeder;

class BookingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      // retrieve all the user_ids from DB
    //   $users = User::all()->lists('id');
    //   foreach(range(1,50) as $index){
    //       $company = Booking::create([
    //           'price' => randomFloat($nbMaxDecimals = NULL, $min = 30, $max = 100),
    //           'user_id' => $faker->randomElement($users),
    //       ]);
    // }
    factory(App\Booking::class, 50)
           ->create();
}
}
