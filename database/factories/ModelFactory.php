<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\countries::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->country,

    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
  $number = sprintf('973%d',$faker->numberBetween($min = 1000000, $max = 99999999));
    return [
        'first_name' => $faker->firstNameMale,
        'last_name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'phone' => $number,
        'country_id' => factory(App\countries::class, 1)->create()->id,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Booking::class, function (Faker\Generator $faker) use ($factory) {
    return [
      'price' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 30, $max = 100),
      'pitch_id' => $faker->numberBetween($min = 1, $max = 90),
      'user_id' => factory(App\User::class, 1)->create()->id

    ];
});
